const dropTable = "DROP TABLE IF EXISTS students;";
const initTable =
  "CREATE TABLE students (" +
  " ID SERIAL PRIMARY KEY," +
  " first_name VARCHAR(255) NOT NULL," +
  " last_name VARCHAR(255) NOT NULL," +
  " email VARCHAR(255) NOT NULL," +
  " date_of_birth DATE NOT NULL," +
  " gender CHAR(1) NOT NULL," +
  " image VARCHAR(1024)," +
  " UNIQUE(email)" +
  " );";

const fillTable =
  "INSERT INTO students" +
  " (first_name, last_name, email, date_of_birth, gender) " +
  " VALUES ('John', 'Doe', 'john.doe@example.com', '1995-05-15', 'M')," +
  " ('Jane', 'Smith', 'jane.smith@example.com', '1996-08-22', 'F')," +
  " ('Michael', 'Johnson', 'michael.johnson@example.com', '1997-02-10', 'M')," +
  " ('Emily', 'Williams', 'emily.williams@example.com', '1998-11-03', 'F')," +
  " ('David', 'Brown', 'david.brown@example.com', '1999-04-25', 'M')," +
  " ('Emma', 'Jones', 'emma.jones@example.com', '2000-07-18', 'F')," +
  " ('Christopher', 'Davis', 'christopher.davis@example.com', '2001-09-30', 'M')," +
  " ('Olivia', 'Miller', 'olivia.miller@example.com', '2002-12-12', 'F')," +
  " ('Daniel', 'Wilson', 'daniel.wilson@example.com', '2003-03-08', 'M')," +
  " ('Sophia', 'Moore', 'sophia.moore@example.com', '2004-06-14', 'F');";

module.exports = {
  dropTable,
  initTable,
  fillTable,
};
