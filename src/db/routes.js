const { Router } = require("express");
const controller = require("./controller");

const router = Router();

router.get("/init", controller.initDb);

module.exports = router;
