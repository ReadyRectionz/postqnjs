const pool = require("../../db");
const queries = require("./queries");

const initDb = (req, res) => {
  pool.query(queries.dropTable, (err, result) => {
    if (!err) {
      pool.query(queries.initTable, (err, result) => {
        if (!err) {
          pool.query(queries.fillTable, (err, result) => {
            if (!err) {
              res.status(200).send("Database initialized successfully");
            } else {
              console.log(err);
              res.status(500).send("Database initialization failed");
            }
          });
        } else {
          console.log(err);
          res.status(500).send("Database initialization failed");
        }
      });
    } else {
      console.log(err);
      res.status(500).send("Database initialization failed");
    }
  });
};

module.exports = { initDb };
