const pool = require("../../db");
const queries = require("./queries");

const studentExistsById = async (id) => {
  let found = false;

  await pool.query(queries.checkIdExistsQuery, [id], (err, result) => {
    found = !err && result.rows.length > 0;
  });
  return found;
};

const studentExistsByEmail = async (email) => {
  let found = false;

  await pool.query(queries.checkEmailExistsQuery, [email], (err, result) => {
    found = !err && result.rows.length > 0;
  });
  return found;
};

const validateEmail = (email) => {
  var emailFormat = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;

  if (email.length < 3 || email.length > 254) {
    return "Email must be between 3 and 254 characters long";
  }
  if (!email.match(emailFormat)) {
    return "Not a valid email format";
  }
};
const validateName = (name) => {
  if (name.length < 3 || name.length > 254) {
    return "Name must be between 3 and 254 characters long";
  }
};
const validateGender = (gender) => {
  if (gender.length < 1 || (gender[0] !== "M") | (gender[0] !== "F")) {
    return "Gender must be either 'M' or 'F'";
  }
};

const getStudents = (req, res) => {
  pool.query(queries.getStudentsQuery, (err, result) => {
    if (err) res.status(500).send({ msg: err.message });
    else res.status(200).json(result.rows);
  });
};

const createStudent = (req, res) => {
  const { first_name, last_name, email, date_of_birth, gender } = req.body;

  if (!studentExistsByEmail(email)) {
    res.status(400).json({ error: `Email '${email}' is already taken` });
    return;
  }

  if (validateEmail(email)) {
    res.status(400).json({ error: validateEmail(email) });
    return;
  }

  if (validateName(first_name)) {
    res.status(400).json({ error: validateName(first_name) });
    return;
  }

  if (validateName(last_name)) {
    res.status(400).json({ error: validateName(last_name) });
    return;
  }

  if (validateGender(gender)) {
    res.status(400).json({ error: validateGender(gender) });
    return;
  }

  const dateOfBirth = new Date(date_of_birth);

  pool.query(
    queries.addStudentQuery,
    [first_name, last_name, email, dateOfBirth, gender],
    (err, result) => {
      if (err) {
        res.status(500).send({ msg: err.message });
      } else {
        res.status(201).json({ msg: "Student created" });
      }
    }
  );
};

const getStudentById = (req, res) => {
  const id = parseInt(req.params.id);

  pool.query(queries.getStudentsByIdQuery, [id], (err, result) => {
    if (err) {
      res.status(500).send({ msg: err.message });
    } else if (result.rows.length <= 0) {
      res
        .status(404)
        .json({ error: `Student with id = ${id} does not exists` });
    } else {
      res.status(200).json(result.rows[0]);
    }
  });
};

const updateStudentById = (req, res) => {
  const id = parseInt(req.params.id);

  if (!studentExistsById(id)) {
    res.status(404).json({ error: `Student with id = ${id} does not exists` });
    return;
  }

  const { first_name, last_name } = req.body;

  if (validateName(first_name)) {
    res.status(400).json({ error: validateName(first_name) });
    return;
  }

  if (validateName(last_name)) {
    res.status(400).json({ error: validateName(last_name) });
    return;
  }
  pool.query(
    queries.updateStudentByIdQuery,
    [first_name, last_name, id],
    (err, result) => {
      if (err) {
        res.status(500).send({ msg: err.message });
      } else {
        res.status(200).json(result.rows[0]);
      }
    }
  );
};

const deleteStudentById = (req, res) => {
  const id = parseInt(req.params.id);

  if (!studentExistsById(id)) {
    res.status(404).json({ error: `Student with id = ${id} does not exists` });
    return;
  }

  pool.query(queries.deleteStudentByIdQuery, [id], (err, result) => {
    if (err) {
      res.status(500).send({ msg: err.message });
    } else {
      res.status(204).json({ msg: "Student deleted" });
    }
  });
};

module.exports = {
  getStudents,
  createStudent,
  getStudentById,
  deleteStudentById,
  updateStudentById,
};
