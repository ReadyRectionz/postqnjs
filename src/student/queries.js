const getStudentsQuery = "SELECT * FROM students";
const getStudentsByIdQuery = "SELECT * FROM students WHERE id = $1";
const checkEmailExistsQuery =
  "SELECT s.id FROM students AS s WHERE s.email = $1";
const checkIdExistsQuery = "SELECT s.id FROM students AS s WHERE s.id = $1";
const deleteStudentByIdQuery = "DELETE FROM students WHERE id = $1";
const addStudentQuery =
  "INSERT INTO students (first_name, last_name, email, date_of_birth, gender) VALUES ($1, $2, $3, $4, $5) RETURNING id";
const updateStudentByIdQuery =
  "UPDATE students SET first_name = $1, last_name = $2 WHERE id = $3 RETURNING *";

module.exports = {
  getStudentsQuery,
  getStudentsByIdQuery,
  checkEmailExistsQuery,
  checkIdExistsQuery,
  addStudentQuery,
  updateStudentByIdQuery,
  deleteStudentByIdQuery,
};
