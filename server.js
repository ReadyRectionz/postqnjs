require("dotenv").config();

const express = require("express");

const studentRoutes = require("./src/student/routes");
const dbRoutes = require("./src/db/routes");

const app = express();

const port = process.env.PORT || 3000;

app.use(express.json());

app.get("/whatiskey", (req, res) => {
  res.header("X-API-KEY", process.env.API_KEY);
  res.json({ msg: "Hello World!" });
});

app.use(function (req, res, next) {
  const apiKey = req.get("X-API-KEY");
  if (apiKey === process.env.API_KEY) {
    next();
  } else {
    res
      .status(401)
      .send(
        "To test the application please set the X-API-KEY header to the correct value"
      );
  }
});

app.get("/", (req, res) => {
  res.json({ msg: "Hello World!" });
});
app.use("/api/v1/students", studentRoutes);
app.use("/db", dbRoutes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

module.exports = app;
