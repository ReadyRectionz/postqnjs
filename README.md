# PostgreSQL NodeJS API

A simple application written in Node.js using PostgreSQL.

### How to run the application

#### Using Kubernetes

`kubectl apply -f k8s/api-deployment.yaml 
-f k8s/postgres-deployment.yaml`

Get the ip of the cluster:

`kubectl cluster-info`

    @ kubectl cluster-info:
    Kubernetes control plane is running at https://192.168.49.2:8443
    CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

Test the API by pinging port 30000 (The default port for the Kubernetes cluster see yaml files)
`wget https://192.168.49.2:30000`

##### Deleting kubectl resources

` kubectl delete -f k8s/api-deployment.yaml 
-f k8s/postgres-deployment.yaml`

#### Using localhost

`mv .env.example .env`

`npm run dev`

`wget localhost:3000`

### Testing API calls

Import the collection by loading the file stored in the `./postman` folder in Postman. The available API calls should then be visible. Update the environment values in postman accordingly to test every call.

![postman_env](./.images/postman_env.png)
